extends RigidBody

signal itemCollected

var speed = 2.5
var isItemCollected = false

export(NodePath) onready var playerRef = get_node(playerRef) as KinematicBody #FETURE
onready var target = playerRef.get_node("coinTailPos")

func _ready():
	pass
	
	
func _process(_delta):
	pass


func _integrate_forces(_state):
#	var playerChilds = playerRef.get_children()
#	target = playerChilds[playerChilds.size() - 1]
	var target_position = target.get_global_transform().origin
	if isItemCollected:
		_follow(get_global_transform(), target_position)
		

func _follow(current_transform, target_position):
	var target_dir = (target_position - current_transform.origin).normalized()
	var vel = target_dir * speed
	if playerRef.isMoving == true:
		set_linear_velocity(vel)
#	else: 
#		set_linear_velocity(Vector3(0,0,0))


func _on_coin_body_entered(body):
	if body.name == "player":
		#FOR MULTIPLAYER
		var playerPath = body.get_path()
		playerRef = get_node(playerPath)
		target = playerRef.get_node("coinTailPos")
		isItemCollected = true
		print("Coin collected!")
		if isItemCollected:
			emit_signal("itemCollected")
			print(get_parent().name)
			
	
#func reparent(child: Node, new_parent: Node):
#	var old_parent = child.get_parent()
#	old_parent.remove_child(child)
#	new_parent.add_child(child)
	
