extends Spatial

enum SpawnPositions {
	TOPLEFT = 1,
	TOPRIGHT = 2,
	DOWNLEFT = 3,
	DOWNRIGHT = 4
}

onready var playerOne = G.subScenes["player"].instance()
onready var itemSpawnPosition = Vector3(0,0,0)

func _ready():
	randomize()
	var randomedPosition = int(rand_range(1,4))
	var playerSpawnPosition: Position3D
	match randomedPosition:
		1:
			playerSpawnPosition = $map/playerSpawnPositions/topLeftSpwnPos
		2:	
			playerSpawnPosition = $map/playerSpawnPositions/topRightSpwnPos
		3:
			playerSpawnPosition = $map/playerSpawnPositions/bottomLeftSpwnPos
		4:
			playerSpawnPosition = $map/playerSpawnPositions/bottomRightSpwnPos
			
	playerOne.global_transform = playerSpawnPosition.global_transform
	add_child(playerOne)
	
func _process(_delta):
	pass

func spawnItems():
	var item = G.subScenes['coin'].instance()
	itemSpawnPosition.x = rand_range(-6, 6)
	itemSpawnPosition.z = rand_range(-6, 6)
	itemSpawnPosition.y = 0.2
	item.playerRef = playerOne.get_path()
	item.connect("itemCollected", self, "_on_item_collected")
	item.set_translation(itemSpawnPosition)
	add_child(item)
	
func _on_spawnItem_timeout():
	spawnItems()
	
func _on_item_collected():
	G.pickedItems += 1

