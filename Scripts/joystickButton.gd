extends TouchScreenButton

var radius = Vector2(62, 62)
var bounds = 124
var ongoingDrag = -1
var return_accel = 30
var threshold = 10

func _process(delta):
	if ongoingDrag == -1:
		var pos_difference = (Vector2(0, 0) - radius) - position
		position += pos_difference * return_accel * delta

func get_button_pos():
	return position + radius

func _input(event):
	if event is InputEventScreenDrag or (event is InputEventScreenTouch and event.is_pressed()):
		var eventDistFromCentre = (event.position - get_parent().global_position).length()

		if eventDistFromCentre <= bounds * global_scale.x or event.get_index() == ongoingDrag:
			set_global_position(event.position - radius * global_scale)

			if get_button_pos().length() > bounds:
				set_position( get_button_pos().normalized() * bounds - radius)

			ongoingDrag = event.get_index()

	if event is InputEventScreenTouch and !event.is_pressed() and event.get_index() == ongoingDrag:
		ongoingDrag = -1

func get_value():
	if get_button_pos().length() > threshold:
		return get_button_pos().normalized()
	return Vector2(0, 0)
