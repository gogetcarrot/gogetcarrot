extends KinematicBody

var accel = 6
var speed = 4
var vel = Vector3()

var grav = -60
var max_grav = -150

onready var joystick = get_node("joystick/joystickButton")
onready var joystickTexture = get_node("joystick")
onready	var player = get_node(".")
onready var coinsTail = get_node("coinTailPos")
var isMoving = false

func _ready():
	joystickTexture.show()
	
func _physics_process(_delta):
	if Input.is_key_pressed(KEY_ESCAPE):
		get_tree().quit()
	
	var target_dir = Vector2(0, 0)
	target_dir = joystick.get_value()
	
	vel.x = lerp(vel.x, target_dir.x * speed, accel * _delta)
	vel.z = lerp(vel.z, target_dir.y * speed, accel * _delta)
	
	vel.y += grav * _delta
	if vel.y < max_grav:
		vel.y = max_grav
	
	vel = move_and_slide(vel, Vector3(0, 1, 0), true, 10)

	if vel.x < 0 || vel.y < 0 || vel.z < 0 || vel.x > 0 || vel.y > 0 || vel.z > 0:
		isMoving = true
	else:
		isMoving = false
		
	if isMoving:
		var angle = atan2(vel.x, vel.z)
		var playerRotation = player.get_rotation()
		playerRotation.y = angle
		player.set_rotation(playerRotation)
