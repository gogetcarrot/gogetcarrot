extends Control

func _ready():
	pass # Replace with function body.

func _process(_delta):
	$pickedItemsLabel.text = String(G.pickedItems)
	$playerScoreLabel.text = String(G.playerScore)
	$timerLabel.text = String(int($gameTimer.time_left))
