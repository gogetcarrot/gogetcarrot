extends Node

var subScenes = {
	'game' : preload("res://Scenes/gameplay.tscn"),
	'player' : preload("res://Scenes/player.tscn"),
	'coin' : preload("res://Scenes/coin.tscn")
}

var playerScore = 0
var pickedItems = 0
var pickedCoinsPos: Array = [Vector3(0,0,0)]
